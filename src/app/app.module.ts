import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {environment} from '../environments/environment';
import {FormsModule} from '@angular/forms';
import { CurdService } from './Firebase/curd.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {AngularFireModule} from '@angular/fire';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {AngularFirestoreModule} from '@angular/fire/firestore';

import { MainComponent } from './main/main.component';




@NgModule({
  declarations: [
    AppComponent,
    
    MainComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFirestoreModule,
  AngularFireModule.initializeApp(environment.firebaseConfig),
    FormsModule
    
  ],
  providers: [CurdService],
  bootstrap: [AppComponent]
})
export class AppModule { }
