import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Route,ActivatedRoute} from '@angular/router';
import { CurdService } from '../Firebase/curd.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  constructor(public router:Router,public Route:ActivatedRoute,public service:CurdService) { }
Id:any;
singleValue:any;
  ngOnInit() {

    this.Id=this.Route.snapshot.paramMap.get('id');
this.service.getDataById(this.Id).subscribe((x:any)=>{
  this.singleValue=x;
})
      
      
  }
  route(){
    this.router.navigate([''])
  }



}
export interface fireData{
  Name:string
  Father:string
  Company:string
  Age:number
  Address:string
}