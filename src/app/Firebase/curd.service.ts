import { Injectable } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';

import { from } from 'rxjs';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CurdService {

  constructor(public fireStore:AngularFirestore) { }

  setValueToFire(value:any){
return this.fireStore.collection('MyData').add(value);
  }

  getEmployee(){
   return this.fireStore.collection('MyData').snapshotChanges();
  }

  updateData(recId:any,rec:any){
    this.fireStore.doc('MyData/' +recId).update(rec);
    
  }

  deleteData(id:any){
    this.fireStore.doc('MyData/'+id).delete();
  }

  getDataById(Id:any){
     
  
   return this.fireStore.doc('MyData/' + Id).valueChanges();

  }

}
