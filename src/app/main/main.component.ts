import { Component, OnInit } from '@angular/core';
import { CurdService } from '../Firebase/curd.service';
import {Router} from '@angular/router'
declare const $:any;
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(public service: CurdService,public router:Router) {}

  
  ngOnInit(){
    this.service.getEmployee().subscribe((data:any)=>{
      
      this.employee=data.map((e:any)=>{
        return{
          id:e.payload.doc.id,
          Name:e.payload.doc.data()['Name'],
          Father:e.payload.doc.data()['Father'],
          Company:e.payload.doc.data()['Company'],
          Age:e.payload.doc.data()['Age'],
          Address:e.payload.doc.data()['Address'],

        }
      }
        
      )
    });

    
  }
  employeeAge: any;
  employeeName: any;
  employeeAddress: any;
  message: any;
  employee:any
  public object: string = '';

  

  setValue() {
    let object = { Name: '', Father:'', Age: '',Company:'', Address: '' };
    object['Name'] = this.employeeName;
    object['Father']=this.employeeFatherName;
    object['Company']=this.Company;
    object['Age'] = this.employeeAge;
    object['Address'] = this.employeeAddress;
this.service.setValueToFire(object);
this.employeeName="";
this.employeeFatherName="";
this.employeeAge="";
this.Company="";
this.employeeAddress="";
    
  }

  editName:any;
  editAge:any;
  editAddress:any;
  employeeFatherName:any;
  Company:any;
  editFather:any;
  editCompany:any;
  Id:any;

 editData(value:any){
   debugger
  this.Id=value.id;
  this.editName=value.Name;
  this.editFather=value.Father;
  this.editCompany=value.Company;
  this.editAge=value.Age;
  this.editAddress=value.Address;

 }
 
 
 showData(value:any){
debugger
  this.router.navigate(['detail', value.id]);
  
  // this.Id=value.id;
  
  // this.editName=value.Name;
  // this.editFather=value.Father;
  // this.editCompany=value.Company;
  // this.editAge=value.Age;
  // this.editAddress=value.Address;

 }



 updateData(){
  debugger

  let updateValue={Name:'',Father:'',Company:'',Age:'',Address:''};
  updateValue.Name=this.editName;
  updateValue.Father=this.editFather;
  updateValue.Company=this.editCompany;
  updateValue.Age=this.editAge;
  updateValue.Address=this.editAddress;
  this.service.updateData(this.Id,updateValue);
  $("#myModal").modal("hide");


 }

 deleteData(item:any){
   this.service.deleteData(item.id);
 }


}
