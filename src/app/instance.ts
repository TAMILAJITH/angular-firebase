export class Instance<T = any> {
    dbName: string
    constructor(name: string) {
      this.dbName = name;
    }
  
    GetAll(): any {
     let value=[];
     value=JSON.parse(localStorage.getItem(this.dbName) || "[]");
     return value;
    }
  
    GetSingleByIndex(i: number) {
      const all = this.GetAll()
      return all[i]
    }
  
    GetSingleById(id: string) {
      const all = this.GetAll()
      return all.find((x:any) => x["_id"] == id)
    }
  
    Update(id: string, ob: any) {
      const all = this.GetAll();
      let index = all.findIndex((x:any) => x["_id"] == id)
      all.splice(index, 1, ob);
      localStorage.setItem(this.dbName, JSON.stringify(all))
      return this.GetSingleById(id)
    }
  
    Create(ob: any) {
      const all = this.GetAll();
      ob["_id"]=this.GetNewID();
    all.push(ob);
      localStorage.setItem(this.dbName, JSON.stringify(all))
      
    }
  
    private gen() {
      let text = "";
      let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  
      for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
  
      return text + (Math.round(Math.random() * 1000)).toString();
    }
  
    private GetNewID(dash: number = 4): string {
      let id = '';
      for (let index = 0; index < dash; index++) {
        id += this.gen() + (dash == index + 1 ? '' : '-')
      }
  
      return id || '';
    }
  
    private generateNumber(): number {
      let text = '';
      let possible = "0123456789";
      for (var i = 0; i < 8; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
  
      return parseInt(text);
    }
  
  }