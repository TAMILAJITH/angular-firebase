// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  
  production: false,
   firebaseConfig : {
    
    apiKey: "AIzaSyAp5LBrltFzA9pGv_vu05heHoxsP4XknPY",
    authDomain: "ui-project-c3b41.firebaseapp.com",
    databaseURL: "https://ui-project-c3b41.firebaseio.com",
    projectId: "ui-project-c3b41",
    storageBucket: "ui-project-c3b41.appspot.com",
    messagingSenderId: "1041522224364",
    appId: "1:1041522224364:web:da88b293bc754576af49c9",
    measurementId: "G-EKZD5N40PE"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
